import React from "react";
import {Link} from "react-router-dom";

import './index.scss'

const Category = (props) => {

    const sidebar = props.sidebar;

    return (
        <div className="category">
            <h2>Мои списки</h2>
            <div className="category_list">
                {sidebar.map((item) => {
                    return (
                        <div className="category_list--item">
                            <Link to={item.link}>{item.name}</Link>
                        </div>
                    )
                })}
            </div>
        </div>
    )

};

export default Category;