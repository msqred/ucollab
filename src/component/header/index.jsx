import React from "react";
import {ReactComponent as Logo} from './../../assets/svg/Logo.svg';
import './index.scss'
import {Link} from "react-router-dom";


const Header = () => {

    return (
        <div className="header">
            <Link to="/" className="header_logo">
                <Logo/>
            </Link>
            <div className="header_info">
                <div className="header_info--btn">New task</div>
                <div className="header_info--avatar">
                    <img src="" alt=""/>
                </div>
            </div>
        </div>
    )

};

export default Header;