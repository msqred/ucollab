import React from 'react'
import ReactDOM from 'react-dom'

import {ApolloClient} from 'apollo-client'
import {HttpLink} from 'apollo-link-http'
import {InMemoryCache} from 'apollo-cache-inmemory'
import {ApolloProvider} from 'react-apollo'
import {Provider} from 'react-redux'
import {createStore} from "redux";

import App from './App';


const GRAPHCMS_API = 'https://api-euwest.graphcms.com/v1/ck4o4mtrm3n8z01bq8xoa04uf/master';

const client = new ApolloClient({
    link: new HttpLink({uri: GRAPHCMS_API}),
    cache: new InMemoryCache()
});


ReactDOM.render(
    <ApolloProvider client={client}>
        <App/>
    </ApolloProvider>,
    document.getElementById('root')
);

