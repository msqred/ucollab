import React from "react";
import {Mutation, Query} from "@apollo/react-components";
import {Link} from "react-router-dom";
import gql from "graphql-tag";


const createAccount = gql`
    mutation createUsers($data: UsersCreateInput!) {
      createUsers(data: $data) {
        email
        password
      }
    }
`;


const Register = (props) => {

    const {setResultMassage, setResultStyle, checkActive, setActive, removeActive, resultStyle, resultMassage, goLogin} = props;
    let inputEmail, inputPassword;


    const submitRegister = (e, createUsers) => {
        e.preventDefault();

        createUsers({
            variables:
                {
                    data:
                        {
                            email: inputEmail.value.toString(),
                            password: parseInt(inputPassword.value, 10)
                        }
                }
        }).then((result) => {
            setResultMassage('Вы успешно зарегистрировались, теперь можете войти в Ваш аккаунт! :)');
            setResultStyle('');
        }).catch((error) => {
            setResultMassage('Пользователь с такими данными уже существует или возникла другая ошибка, попробуйте позже');
            setResultStyle('error');
        });
    }


    return (
        <div className="login_form">
            <h1 className="up">Регистрация</h1>
            <Mutation mutation={createAccount}>
                {(createUsers, {data}) => (

                    <form
                        autoComplete="off"
                        onSubmit={(e) => submitRegister(e, createUsers)}
                    >
                        <div className="input_wrap up">
                            <input
                                type="email"
                                name="email"
                                required
                                ref={node => {
                                    inputEmail = node
                                }}
                                onChange={checkActive}
                                onFocus={setActive}
                                onBlur={removeActive}
                            />
                            <label htmlFor="nickName">
                                Ваш E-mail
                            </label>
                        </div>

                        <div className="input_wrap up">
                            <input
                                type="password"
                                name="password"
                                required
                                ref={node => {
                                    inputPassword = node
                                }}
                                onChange={checkActive}
                                onFocus={setActive}
                                onBlur={removeActive}
                            />
                            <label htmlFor="nickName">
                                Ваш пароль
                            </label>
                        </div>

                        <button
                            type="submit"
                            className="up"
                        >
                            <span>Зарегистрироваться</span>
                        </button>
                    </form>
                )}
            </Mutation>
            <div className={`result_massage ${resultStyle === 'error' ? 'error' : ''}`}>
                {resultMassage}
            </div>
            <div className="no_account up">
                <span>Уже есть аккаунт?</span>
                <Link to='/'
                      onClick={goLogin}
                >
                    Войти
                </Link>
            </div>
        </div>
    )
}

export default Register;