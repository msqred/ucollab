import React, {useEffect, useState} from "react";
import {useLazyQuery} from '@apollo/react-hooks';
import {Link} from "react-router-dom";
import gql from "graphql-tag";


const loginToAccount = gql`
    query loginAccount($email: String) {
      users(where: {email: $email}) {
        id
        email
        password
      }
    }
`;


const Login = (props) => {

    const {checkActive, setActive, removeActive, goRegister, setLoggedIn} = props;
    const [resultMassage, setResultMassage] = useState('');
    const [resultStyle, setResultStyle] = useState('');
    let inputEmail, inputPassword;

    const [getUser, {loading, data}] = useLazyQuery(loginToAccount);

    useEffect(() => {
        checkLogin();
    }, [data])

    const submitLogin = (e) => {
        e.preventDefault();
        getUser({
            variables:
                {
                    email: inputEmail.value.toString()
                }
        });
        checkLogin();
    }

    const checkLogin = () => {
        if (data !== undefined && data.users !== null) {
            if (parseInt(data.users.password, 10) === parseInt(inputPassword.value, 10)) {
                console.log(inputPassword.value);
                setResultMassage('Пароли совпадают');
                setResultStyle('');
                setLoggedIn(true);
                window.localStorage.setItem('auth', 'true');
            } else {
                console.log(inputPassword.value);
                setResultMassage('Пароль не верный');
                setResultStyle('error');
                window.localStorage.setItem('auth', 'false');
            }
        }
    }


    return (
        <div className="login_form">
            <h1 className="up">Войдите в аккаунт</h1>
            <form autoComplete="off"
                  onSubmit={(e) => submitLogin(e)}
            >
                <div className="input_wrap up">
                    <input
                        type="email"
                        name="email"
                        required
                        ref={(node) => {
                            inputEmail = node
                        }}
                        onChange={checkActive}
                        onFocus={setActive}
                        onBlur={removeActive}
                    />
                    <label htmlFor="nickName">
                        Ваш E-mail
                    </label>
                </div>

                <div className="input_wrap up">
                    <input
                        type="password"
                        name="password"
                        required
                        ref={(node) => {
                            inputPassword = node
                        }}
                        onChange={checkActive}
                        onFocus={setActive}
                        onBlur={removeActive}
                    />
                    <label htmlFor="nickName">
                        Ваш пароль
                    </label>
                </div>

                <button
                    type="submit"
                    className="up"
                >
                    <span>Войти</span>
                </button>
            </form>
            <div className={`result_massage ${resultStyle === 'error' ? 'error' : ''}`}>
                {resultMassage}
            </div>
            <div className="no_account up">
                <span>Нет аккаунта?</span>
                <Link to='/'
                      onClick={goRegister}
                >
                    Зарегистрироваться
                </Link>
            </div>
        </div>
    )
}


export default Login;