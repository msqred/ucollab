import React, {useState, useEffect} from 'react';
import ScrollReveal from 'scrollreveal';
import {Helmet} from "react-helmet";

import {ReactComponent as Logo} from './../../assets/svg/Logo.svg';
import './index.scss'
import Login from "./Login";
import Register from "./Register";


const LoginPage = (props) => {

    const {setLoggedIn} = props;

    useEffect(() => {
            scrollReveal()
        }, []
    );


    const [page, setPage] = useState('login');
    const [resultMassage, setResultMassage] = useState('');
    const [resultStyle, setResultStyle] = useState('');


    const checkActive = (e) => {
        if (e.target.value !== '') {
            e.target.parentElement.classList.add('active');
        } else {
            e.target.parentElement.classList.remove('active');
        }
    };

    const setActive = (e) => {
        e.target.parentElement.classList.add('active');
    };

    const removeActive = (e) => {
        if (e.target.value !== '') {
            e.target.parentElement.classList.add('active');
        } else {
            e.target.parentElement.classList.remove('active');
        }
    }

    const goRegister = () => {
        setPage('register')
    };

    const goLogin = () => {
        setPage('login')
    };


    function scrollReveal() {

        ScrollReveal().reveal('.up', {
            distance: '100px',
            interval: 200,
            duration: 2000,
            easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)',
            delay: 100,
            opacity: 0
        });

        ScrollReveal().reveal('.scale', {
            easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)',
            duration: 2000,
            scale: 0
        });
    }

    return (
        <React.Fragment>
            <Helmet>
                <meta charSet="utf-8"/>
                <title>uCollab - manage your task in one place.</title>
            </Helmet>
            <div className="login_page">
                <div className="container">
                    <div className="logo scale">
                        <Logo/>
                    </div>
                    {page === 'login' ?
                        <Login
                            checkActive={checkActive}
                            setActive={setActive}
                            removeActive={removeActive}
                            goRegister={goRegister}
                            setLoggedIn={setLoggedIn}
                        />
                        :
                        <Register
                            setResultMassage={setResultMassage}
                            setResultStyle={setResultStyle}
                            checkActive={checkActive}
                            setActive={setActive}
                            removeActive={removeActive}
                            goRegister={goRegister}
                            resultMassage={resultMassage}
                            resultStyle={resultStyle}
                            goLogin={goLogin}
                        />

                    }

                </div>
            </div>
        </React.Fragment>
    )
};

export default LoginPage;
