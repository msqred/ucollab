import React, {useState, useEffect} from 'react';
import {Helmet} from "react-helmet";
import Header from "../../component/header";
import './index.scss'
import {Link} from "react-router-dom";
import Category from "../../component/category";

const Sidebar = [
    {
        name: 'Задачи',
        link: '/tasks'
    },
    {
        name: 'Чат',
        link: '/chat'
    }
];


const Dashboard = () => {


    return (
        <React.Fragment>
            <Helmet>
                <meta charSet="utf-8"/>
                <title>uCollab - dashboard</title>
            </Helmet>
            <Header/>
            <div className="app-container">
                <Category
                    sidebar={Sidebar}
                />
            </div>

        </React.Fragment>
    )
};

export default Dashboard;
