import React, {useEffect, useState} from 'react'
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom'
import LoginPage from "./pages/loginPage";

import './assets/styles/index.scss';
import Dashboard from "./pages/dashboard";


const App = () => {


    return (
        <Router>

            <React.Fragment>

                <Switch>
                    <Route exact path="/" component={LoginPage}/>
                    <Route path="/app" component={Dashboard}/>
                </Switch>
            </React.Fragment>
        </Router>
    )
};

export default App;
